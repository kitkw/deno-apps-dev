# webjs

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build webjs` to build the library.

## Running unit tests

Run `nx test webjs` to execute the unit tests via [Jest](https://jestjs.io).
